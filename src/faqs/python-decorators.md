# Python decorators
***by `Tmpod#2525`***

---

Welcome to this little guide on a very important feature of Python - decorators!

I will be explaining what decorators are, why they are useful, and how to define and use them. Let's get started!


### What are decorators?

Before I tell you what they are exactly, we need to understand how stuff is defined in Python.
In basic terms, you can create variables (`foo = "bar"`), functions (`def foo(): ...`) and classes (`class Foo: ...`),
but contrary to what beginners might think, both functions and classes are just variables and behave like them.
What I mean with this is that you can pass functions and classes around to other functions, like primitive variables
 or copy their reference, or even inspect their properties, like
[`__name__`](https://docs.python.org/3/library/stdtypes.html?highlight=__name__#definition.__name__) (which returns the
function/class name).t

Decorators are normal functions, but they take a function or a class as its single argument and return something.
They are [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar), and they were conceived to make writing
some things easier.

If we have this function
```py
def print_name(target):
    print(target.__name__)
    return target
```
*I'll be covering how to write decorators in another section, don't worry about the details for now*

```py
def foo():
    ...

foo = print_name(foo)
```
is equivalent to
```py
@print_name
def foo():
    ...
```

In short, if we want to apply some logic to a function or class, instead of doing a reassignment
(`foo = print_name(foo)`), we can just use the `@` notation to use the function as a decorator and place it above our
target, effectively *decorating* it.


### Why are they useful?

Decorators are a very powerful feature that often come in handy, and are used by a lot of libraries and programs.
They are generally used when you need to apply a certain transformation to a function or class, or perform something
whenever they are defined.
For example, in `discord.py` and `hikari` (two Discord bot libraries) you use a decorator to mark functions as event
handlers:

```py
# Using hikari

@bot.on(hikari.MessageCreateEvent)
async def do_something_on_every_message(event: hikari.MessageCreateEvent):
    """Does something interesting with every message."""
    ...
```

In this case, the decorator will register the `do_something_on_every_message` function as a handler for the
`MessageCreateEvent` event, so it can run it whenever the bot receives a message.

Another example is the way you define commands on `discord.py` and `hikari-lightbulb`. This time we're not just
registering a function, but also applying a transformation to it. Look:

```py
# Using lightbulb

@bot.command(name="foo")
async def foo_command(ctx: lightbulb.Context):
    """Very cool command."""
    ...
```

In this case, the library will transform the function into a `Command` object, which will then be registered onto the
bot. So if you then reference `foo_command`, you'll in fact have a `Command` and not a function.

Finally, you can also see decorators being used in Python's standard library and builtins, with decorators like
`@staticmethod`, `@classmethod` and a lot of functions in the `functools` module.


### Alright, this is all very nice, but how can I make my own decorators?

It's very simple!
As I've mentioned in the first section, decorators are nothing more than functions that take functions or classes as
arguments and return something, preferrably functions or classes. The argument will be whatever results of the decorated
function/class definition, and whatever you return will replace its value.

Let's start with a simple example. Let's make a decorator that prints out the name of the functions/classes it is
decorating.

```py
def print_name(target):
    print(target.__name__)
    return target
```

So, what does this do exactly? Well, we define a function that takes an argument `target` (which should be a function or
a class), and it prints its name using the special `__name__` property.
We then return the target again, else it would be replaced with `None`, since that's the default return value.

Let's try it:

*using the REPL*
```py
>>> @print_name
    def foo():
        ...

foo
>>> @print_name
    def bar():
        ...

bar
```

As we can see, it printed the function names as we defined them.

This is a very simple, and frankly not very useful decorator. What if we want to do something more complex?
What if we want arguments to "customise" what we do with the decorated functions or classes, like what I showed with
the Discord bot examples?

For that we need to use something called "decorator generators", which sounds a bit more fancy but it's nothing to fear.
Decorator generators are simply functions that can take whatever arguments you want and return a decorator which can
make use of said arguments. Let's see an example:

```py
def print_name_with_prefix(prefix):
    # Inner function that will actually be the decorator and is able to use the prefix argument
    def decorator(target):
        print(prefix, target.__name__)
        return target

    return decorator  # Don't call it
```

*now trying it in the REPL*
```py
>>> @print_name_with_prefix("->")
    def foo():
        ...

-> foo
>>> @print_name_with_prefix("::")
    def bar():
        ...

:: bar
```

We can see it printed the function names similarly to last time, but now it prefixed them with the string we passed onto
the decorator generator.

This works by using inner functions that can use the outside function's arguments. In this case, we define a the
`decorator` function inside the wrapping `print_name_with_prefix` function, and so it can use the `prefix` argument.

In essence, `@print_name_with_prefix("->")` will be replaced with `@decorator`, then `foo` (the decorated function) will
be consumed, `-> foo` will be printed, and `foo` is gonna be returned to its place.


### Conclusion

Hopefully you now have a better understanding of what decorators are and how to use them, which should make it easier
to turn your ideas into programs!
Don't forget to also check the Python documentation,
like [this section](https://docs.python.org/3/glossary.html#term-decorator) in the glossary.
If you have any remaining question feel free to hop onto our server and leave a message
in either **`#py-help-0`** or **`#py-help-1`** 🙂
