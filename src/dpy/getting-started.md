# Getting Started

Hey! So, to begin working with `discord.py` you'll obviously need to have Python installed and to install the library. For a guide on how to properly install Python and use venvs go to [**Installing Python**](../faqs/installing-python.html) and [**Python Venvs**](../faqs/python-venvs.html), respectively. To install the library you just have to do `pip install discord.py` (if you are not using a venv, don't forget to change the pip version accoordingly). If you want to include the part for audio transmission support you need to type in `pip install discord.py[voice]` instead.

Now that you got your venv setup and your dependencies installed, it's time to start coding!   
Go grab your bot token and let's start!  
First create a text file and just paste your token there. Like this:  
`token.txt`
```
NTI2NTIxMzIyOTI3NDIzNDkx.XP_KqA.h7v_KI4P7Qwh1Lp5ZbIuSuJ7Ygs
```
*that token is obviously fake...*

And now create a file called something like `main.py`. This is where you'll create a working connection to Discord.  
`main.py`
```python
import discord

# You need to adjust this path to fit your project structure
# If you have the token file in the same dir as this one, 
# the path should be just 'token.txt'
with open("path/to/token.txt") as fp:
    TOKEN = fp.read().strip()

client = discord.Client()

client.run(TOKEN)
```
This will create and run a client that will provide you with the barebones of a usable connection to the Discord API. With this base client you'll be able to receive events (such as a new message, new member on a guild or even a ban!) and send requests to the API in order to post messages, add reactions or kick someone!  

> __IMPORTANT:__ Do not forget to add the token file to your `.gitignore` if you are using Git, as not doing so will most likely lead to you leaking the token which basically gives a free white pass to anyone that wants to cause destruction.

---

Based on this client, `discord.py` offers a more complex module that allows you to easily create and manage commands - `discord.ext.commands`. With it, making bots becomes way easier and more manageable. Cool!  
To start using this module, you'll just need to modify a bit the code from before. Here's an example:  
`main.py`
```python
from discord.ext.commands import Bot

with open("path/to/token.txt") as fp:
    TOKEN = fp.read().strip()

bot = Bot(command_prefix="bot!")

bot.run(TOKEN)
```
As you can see, we now have to pass an argument called `command_prefix`, wich will define which char or sequence of characters will trigger the bot's commands. For instance, with the prefix shown in the codeblock above, I'd have to type `bot!somecommand` into Discord to call a command named `somecommand`. It's important to make that prefix unique as it may get really messy really fast on servers with a lot of bots. Try avoiding common characters like `!`, `?`, `-` or `.` and go for a combination of your bot's initials followed by one of those characters instead, i.e. if my bot was called "FooBar", I'd make its prefix be `fb.` . There're also ways to make customisable prefixes which are bit more complex to setup and are covered in the [**Useful Need-To-Knows**](./need-to-knows.html#customisable-prefixes).  

You now have a working bot! 🎉  
To run it just type `python main.py` in your project folder and the bot should just come online.  

---

For now it doesn't do much, but if you follow me to [**Processing Events**](./processing-events.html) I'll show you how you can start making your bot do useful stuff. See you there!
