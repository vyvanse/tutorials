# Coding the bot

Okay, now, to finally coding the bot!
We'll create the main class.

First things first, in the file viewer, expand `src` and expand `main` within it, and right-click on Java then hover over `New`, and click on `Java Class`.

![Creating Java Class](./imgs/creating_java_class.jpg)


Call your main class any name and click `Ok` like this:

![Naming Java Class](./imgs/naming_java_class.jpg)

Now create a main method in the class you just created, `Hint: type psvm<tab>`.

![Creating main method](./imgs/creating_main_method.jpg)

Now we're going to create our JDA instance in the main method!

Though, first we're going to have to learn something about the `JDABuilder` class.

The class used to be initialized using `new JDABuilder("token").build()` which now is deprecated. Along with Discord's new-coming intent system, a new way is now used to create your JDA instance and its using the [`JDABuilder#create...()` methods](https://ci.dv8tion.net/job/JDA/javadoc/net/dv8tion/jda/api/JDABuilder.html), which in you can customize your intents & also need to put your token in (as well as cache settings for members and guilds, etc.).

In this tutorial, we're going to use the [`createDefault` method](https://ci.dv8tion.net/job/JDA/javadoc/net/dv8tion/jda/api/JDABuilder.html#createDefault(java.lang.String,net.dv8tion.jda.api.requests.GatewayIntent,net.dv8tion.jda.api.requests.GatewayIntent..)) which gives you the recommended intents (and you can still enable other intents) as well as recommended cache settings, but you can use any other creator method you prefer.

> Notice that the creator methods are static!

In the main method, type:

```java
JDA jda = JDABuilder.createDefault("token")
    .build();
// It'd be better if you actually kept it in a
// `.txt` or `.json` config file and grabbed
// the token from there.

// Blocks the bot until JDA is fully ready.
jda.awaitReady();
```

This will create a JDA instance for your bot. If some lines are red, they may be because of the imports missing, it should be in the quickfix menu if you right-click on the error.

We are going to add `throws LoginException` right after our main method and before the curly bracket.

The full code would look like this:

```java
public class Main {
    public static void main(String[] args) throws LoginException, InterruptedException {
        JDA jda = JDABuilder.createDefault("token")
            .build();

        jda.awaitReady();
    }
}
```

The `LoginException` is in case something wrong happens while logging in with the token.

And the `InterruptedException` is in case the bot gets interrupted when its blocking until its ready.

> **IMPORTANT:** BE SURE NOT TO GIVE THE TOKEN TO ANYONE. NOT EVEN YOUR MOM!

So that's why I'm going to teach you how to make your token a little bit more secure!
First, make a new file anywhere you want. Call it `token.txt` and don't put anything in it other than the token.
And then, we would need to read the token from the file. We'll use the packages `java.util.Scanner` and `java.io.File` to do it.

We would need to try to read the file in a try/catch block, and assign the file to a variable, like this:

```java
File tokenFile = new File("C:\users\user\Documents\token.txt");

String token = null;

try(Scanner s = new Scanner(tokenFile)) {
    if(s.hasLine()) {
        token = s.hasNextLine();
    } else {
        throw new RuntimeException("The token file is empty");
    }
} catch (IOException e) {
    System.out.println("File not found");
}
```

Use this code just in the main method before the JDA instance!

> **NOTICE:** In here we should create a file and put it anywhere we like, so you'd need to use your `token.txt`'s path wherever you put it!

```java
public static void main(String[] args) throws LoginException {
    JDA jda = JDABuilder.createDefault(token)
        .build();

    jda.awaitReady();
}
```

There we are!


Now, click the green play button to the left of our class name and select `Run`.

You'll see something like this:

![Expected Results](./imgs/first_run_expected_results.jpg)

The first 6 lines are because we don't have any `slf4j` implementations, *yet*.

The next 3 lines tell us that JDA has successfully logged in to Discord, and is ready to receive messages. But our bot doesn't do anything right now.

Now look in the guild that your bot is in! If it's online, HOORAY! Everything is working! If it's not, follow the tutorial and check the code out again or ask in [**`jvm-help`**](https://discordapp.com/channels/265828729970753537/651854967778443265) in our server's channel!

---

Let's move on to [**Commands, Listeners and Activities**](./commands-listeners-and-activities.html).
