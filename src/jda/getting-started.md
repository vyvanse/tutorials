# Getting Started

Now, first, run the IntelliJ IDEA IDE that you installed, You'll see this screen:

![Main Screen IntelliJ](./imgs/main_screen_intellij.jpg)

Click on `Create New Project`.

Then select `Gradle` (or `Maven`), and just Mark `Java`, like this:

![Creating New Project](./imgs/new_project_creation.jpg)

Then click `Next`.

After that, choose a group and artifact ID, they can be anything you want, but usually, the group ID is the reverse of a domain you own, so `mywebsite.com` becomes `com.mywebsite` and the artifact ID is an identifier for the project, such as `my-jda-bot`, like this:

![Naming the Artifact ID](./imgs/naming_artifact_id.jpg)

Then click `Next`.

You don't need to change the Gradle/Maven settings. You can just click `Next`.

After that, just choose a name for the project and where to save it, and click on `Finish`:

![Naming New Project](./imgs/new_project_naming.jpg)

Now wait for Gradle to finish configuring your project, after its done, you'd see this screen (different folders may appear, don't worry, don't get confused):

![Editor view](./imgs/intellij_editor_view.jpg)

Now, open `build.gradle` (`pom.xml` if you chose Maven) and then copy-paste the paragraph that is in the Installation header under the dependency management language you picked in the newest version from [here](https://github.com/DV8FromTheWorld/JDA/releases).

![Adding the dependencies](./imgs/adding_dependencies.jpg)

> **IMPORTANT:** The version in the picture is outdated, so use the newest version you can find it [here](https://github.com/DV8FromTheWorld/JDA/releases), like I said earlier.


![Gradle import pop-up](./imgs/gradle_import_pop-up.jpg)

If you see a dialog like this (it might be only occuring to Gradle users,) click `Import Changes`.

---

Let's move on to [**Coding the bot**](./coding-the-bot.html).
